\babel@toc {american}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Metals}{2}{chapter.2}%
\contentsline {section}{\numberline {2.1}Electronic Properties of Metals}{2}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Semi Metals}{2}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Typical Conductivity $\sigma $ , Resistivity $\rho $ and Electron Density $\eta $}{2}{subsection.2.1.2}%
\contentsline {section}{\numberline {2.2}Drude and Sommerfeld Models}{3}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Drude Model}{3}{subsection.2.2.1}%
\contentsline {subsubsection}{\nonumberline Derive expression for DC conductivity $\sigma $}{3}{section*.3}%
\contentsline {subsubsection}{\nonumberline Hall effect}{4}{section*.4}%
\contentsline {subsubsection}{\nonumberline Failures of the Drude Model}{5}{section*.5}%
\contentsline {subsection}{\numberline {2.2.2}Sommerfeld Model}{5}{subsection.2.2.2}%
\contentsline {chapter}{\numberline {3}Periodic Potential}{7}{chapter.3}%
\contentsline {section}{\numberline {3.1}The periodic potential}{7}{section.3.1}%
\contentsline {section}{\numberline {3.2}Periodic boundary conditions for transport}{7}{section.3.2}%
\contentsline {section}{\numberline {3.3}Schrödinger equation with a periodic potential}{8}{section.3.3}%
\contentsline {section}{\numberline {3.4}Very weak periodic potential}{9}{section.3.4}%
\contentsline {subsection}{\numberline {3.4.1}Single electron case}{10}{subsection.3.4.1}%
\contentsline {subsection}{\numberline {3.4.2}Two degenerate free-electron levels}{10}{subsection.3.4.2}%
\contentsline {section}{\numberline {3.5}The tight binding model}{12}{section.3.5}%
\contentsline {subsection}{\numberline {3.5.1}A band arising from a single electronic level}{12}{subsection.3.5.1}%
\contentsline {section}{\numberline {3.6}Significance of the electron wave vector $\vec {k}$}{13}{section.3.6}%
\contentsline {section}{\numberline {3.7}A first look at Transport}{14}{section.3.7}%
\contentsline {subsection}{\numberline {3.7.1}of how to tackle transport}{15}{subsection.3.7.1}%
\contentsline {section}{\numberline {3.8}The notion of holes}{16}{section.3.8}%
\contentsline {chapter}{\numberline {4}Semiconductors, insulators and band structure engineering}{17}{chapter.4}%
\contentsline {section}{\numberline {4.1}Indirect band gap semiconductors: Si, Ge}{17}{section.4.1}%
\contentsline {section}{\numberline {4.2}Direct band gap semiconductors}{18}{section.4.2}%
\contentsline {section}{\numberline {4.3}Indirect band gap semiconductors: Si, Ge}{18}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Exitons}{19}{subsection.4.3.1}%
\contentsline {section}{\numberline {4.4}Thermal population of bands}{20}{section.4.4}%
\contentsline {section}{\numberline {4.5}Impurities}{21}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Extrinsic carrier density}{21}{subsection.4.5.1}%
\contentsline {subsection}{\numberline {4.5.2}Chemical potential and impurities}{22}{subsection.4.5.2}%
\contentsline {subsection}{\numberline {4.5.3}Degenerate semiconductors}{22}{subsection.4.5.3}%
\contentsline {section}{\numberline {4.6}Band structure engineering}{22}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Heterojunction}{23}{subsection.4.6.1}%
\contentsline {subsection}{\numberline {4.6.2}Quantum well}{23}{subsection.4.6.2}%
\contentsline {subsubsection}{\nonumberline Superlattices}{23}{section*.7}%
\contentsline {subsection}{\numberline {4.6.3}Organic molecules}{23}{subsection.4.6.3}%
\contentsline {subsection}{\numberline {4.6.4}Fermi surfaces of layered materials}{24}{subsection.4.6.4}%
\contentsline {subsubsection}{\nonumberline Peierls theorem}{24}{section*.8}%
\contentsline {chapter}{\numberline {5}Quantization of electronic motion in magnetic fields}{25}{chapter.5}%
\contentsline {section}{\numberline {5.1}Lorentz force and cyclotron frequency}{25}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Derivation of the cyclotron frequency}{25}{subsection.5.1.1}%
\contentsline {section}{\numberline {5.2}Introduction to Landau Levels}{26}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Energies of the electrons}{26}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Arbitrarily shaped bands and the Bohr correspondence principle}{28}{subsection.5.2.2}%
\contentsline {subsubsection}{\nonumberline Effect of the Landau quantization on the density of states}{29}{section*.9}%
\contentsline {section}{\numberline {5.3}Quantum oscillations}{29}{section.5.3}%
\contentsline {section}{\numberline {5.4}Intra- and Interband magneto-optics}{30}{section.5.4}%
\contentsline {subsubsection}{\nonumberline Iterband magnetoptics in semiconductors}{30}{section*.11}%
\contentsline {section}{\numberline {5.5}Beyond the independent-electron approximation: Fermi liquid theory}{31}{section.5.5}%
\contentsline {chapter}{\numberline {6}Elementary transport theory}{32}{chapter.6}%
\contentsline {section}{\numberline {6.1}Kinetic theory of transport in metals}{33}{section.6.1}%
\contentsline {subsection}{\numberline {6.1.1}Recap of the relaxation time approximation of the Drude and Sommerfeld models}{33}{subsection.6.1.1}%
\contentsline {subsection}{\numberline {6.1.2}Electrical conduction and the Fermi surface}{34}{subsection.6.1.2}%
\contentsline {subsection}{\numberline {6.1.3}Thermal conduction and the Fermi surface}{35}{subsection.6.1.3}%
\contentsline {subsection}{\numberline {6.1.4}Different scattering processes and Mattheissen's rule}{35}{subsection.6.1.4}%
\contentsline {subsubsection}{\nonumberline Electron phonon scattering}{36}{section*.12}%
\contentsline {subsubsection}{\nonumberline Electron and phonon scattering at room Temperature}{36}{section*.13}%
\contentsline {subsubsection}{\nonumberline Electron and phonon scattering at low Temperature}{36}{section*.14}%
\contentsline {subsection}{\numberline {6.1.5}Thermal conduction and the Fermi surface}{36}{subsection.6.1.5}%
\contentsline {subsection}{\numberline {6.1.6}Thermal conduction and the Fermi surface}{37}{subsection.6.1.6}%
\contentsline {subsubsection}{\nonumberline Very low Temperatures}{37}{section*.15}%
\contentsline {subsection}{\numberline {6.1.7}Electron electron scattering}{37}{subsection.6.1.7}%
\contentsline {section}{\numberline {6.2}Electrical conductivity of semiconductors}{38}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Temperature dependence of the carrier densities}{38}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Temperature dependence of the mobilities}{38}{subsection.6.2.2}%
\contentsline {chapter}{\numberline {7}The Hall effect and magneto resistance}{40}{chapter.7}%
\contentsline {section}{\numberline {7.1}Introduction to the Hall effect}{40}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}Geometry of the Hall effect}{40}{subsection.7.1.1}%
\contentsline {subsection}{\numberline {7.1.2}Review of the Hall effect in the Drude model}{40}{subsection.7.1.2}%
\contentsline {subsection}{\numberline {7.1.3}The Hall effect with more than one charge carrier}{40}{subsection.7.1.3}%
\contentsline {subsection}{\numberline {7.1.4}Hall effect in the high field limit}{42}{subsection.7.1.4}%
\contentsline {section}{\numberline {7.2}Tensorial description of magnetoresistance}{42}{section.7.2}%
\contentsline {section}{\numberline {7.3}Quantum effects}{44}{section.7.3}%
\contentsline {subsection}{\numberline {7.3.1}Some examples}{44}{subsection.7.3.1}%
\contentsline {subsubsection}{\nonumberline Recap of Landau quantization in 3D}{44}{section*.16}%
\contentsline {chapter}{Bibliography}{46}{chapter*.17}%
