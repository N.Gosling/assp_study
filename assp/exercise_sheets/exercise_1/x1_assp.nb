(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     17165,        418]
NotebookOptionsPosition[     15915,        388]
NotebookOutlinePosition[     16310,        404]
CellTagsIndexPosition[     16267,        401]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"-", "x"}], "\[Mu]"]], ",", 
       FractionBox["1", 
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          FractionBox[
           RowBox[{"x", "-", "\[Mu]"}], "\[Mu]"]], "+", "1"}]], ",", 
       FractionBox["1", 
        RowBox[{
         SuperscriptBox["\[ExponentialE]", 
          FractionBox[
           RowBox[{"x", "-", "\[Mu]"}], "\[Mu]"]], "-", "1"}]]}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "10"}], "}"}], ",", 
     RowBox[{"PlotLegends", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
       "\"\<Boltzmann\>\"", ",", "\"\<Fermi-Dirac\>\"", ",", 
        "\"\<Bose-Einstein\>\""}], "}"}]}], ",", 
     RowBox[{
     "PlotLabel", "\[Rule]", 
      "\"\<Distributions for \[Mu]=\!\(\*SubscriptBox[\(k\), \
\(B\)]\)T=1\>\""}], ",", 
     RowBox[{"AxesLabel", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"\"\<E\>\"", ",", "\"\<<n>\>\""}], "}"}]}], ",", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", "10"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"-", "0.2"}], ",", "1.5"}], "}"}]}], "}"}]}]}], "]"}], ",", 
   
   RowBox[{"{", 
    RowBox[{"\[Mu]", ",", "1"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.8140115032035227`*^9, 3.8140116132093267`*^9}, {
  3.8140116705047417`*^9, 3.814011704391021*^9}, {3.81401183535688*^9, 
  3.814011951928125*^9}, {3.814011996985034*^9, 3.8140120394392138`*^9}},
 CellLabel->"In[11]:=",ExpressionUUID->"06a9b235-62d8-4a55-80ce-eb744f449acb"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`\[Mu]$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`\[Mu]$$], 1}}, Typeset`size$$ = {490., {117., 122.}}, 
    Typeset`update$$ = 0, Typeset`initDone$$, Typeset`skipInitDone$$ = True}, 
    
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`\[Mu]$$ = 1}, 
      "ControllerVariables" :> {}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[{E^((-$CellContext`x)/$CellContext`\[Mu]$$), 1/(
         E^(($CellContext`x - $CellContext`\[Mu]$$)/$CellContext`\[Mu]$$) + 
         1), 1/(E^(($CellContext`x - \
$CellContext`\[Mu]$$)/$CellContext`\[Mu]$$) - 1)}, {$CellContext`x, 0, 10}, 
        PlotLegends -> {"Boltzmann", "Fermi-Dirac", "Bose-Einstein"}, 
        PlotLabel -> 
        "Distributions for \[Mu]=\!\(\*SubscriptBox[\(k\), \(B\)]\)T=1", 
        AxesLabel -> {"E", "<n>"}, PlotRange -> {{0, 10}, {-0.2, 1.5}}], 
      "Specifications" :> {{$CellContext`\[Mu]$$, 1}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{535., {160., 166.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.81401160679784*^9, 3.814011613761469*^9}, 
   3.814011706296954*^9, {3.814011845982177*^9, 3.814011901495634*^9}, 
   3.8140119521396017`*^9, {3.814012007010549*^9, 3.81401204147856*^9}},
 CellLabel->"Out[11]=",ExpressionUUID->"4922b6a0-92f7-4c27-9452-38b6bf9eebfa"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"-", "x"}], 
         RowBox[{"0.01", "k"}]]], ",", 
       SuperscriptBox["\[ExponentialE]", 
        FractionBox[
         RowBox[{"-", "x"}], 
         RowBox[{"1000", "k"}]]]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "10"}], "}"}], ",", 
     RowBox[{"PlotRange", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"{", 
         RowBox[{"0", ",", "10"}], "}"}], ",", 
        RowBox[{"{", 
         RowBox[{"0", ",", "1"}], "}"}]}], "}"}]}], ",", 
     RowBox[{"PlotLegends", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"\"\<Low Temperature\>\"", ",", "\"\<High Temperature\>\""}], 
       "}"}]}], ",", 
     RowBox[{"PlotLabel", "\[Rule]", "\"\<Boltzmann Distributions\>\""}], ",", 
     RowBox[{"AxesLabel", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{"\"\<E\>\"", ",", "\"\<<n>\>\""}], "}"}]}]}], "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"k", ",", "1", ",", "100"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.813835285618436*^9, 3.8138353486269283`*^9}, 
   3.814011495544735*^9, {3.814012451277215*^9, 3.814012548977528*^9}, {
   3.814012586713277*^9, 3.8140125898484783`*^9}, {3.814012730013709*^9, 
   3.814012741005909*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"e1b2e79d-4bff-4fae-b653-0bc54b962a4d"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`k$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`k$$], 1, 100}}, Typeset`size$$ = {511., {124., 128.}},
     Typeset`update$$ = 0, Typeset`initDone$$, Typeset`skipInitDone$$ = True}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`k$$ = 1}, 
      "ControllerVariables" :> {}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[{E^(-$CellContext`x/(0.01 $CellContext`k$$)), 
         E^(-$CellContext`x/(1000 $CellContext`k$$))}, {$CellContext`x, 0, 
         10}, PlotRange -> {{0, 10}, {0, 1}}, 
        PlotLegends -> {"Low Temperature", "High Temperature"}, PlotLabel -> 
        "Boltzmann Distributions", AxesLabel -> {"E", "<n>"}], 
      "Specifications" :> {{$CellContext`k$$, 1, 100}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{556., {169., 175.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.8138353011891937`*^9, 3.813835348947489*^9}, 
   3.814012518494812*^9, 3.814012558115149*^9, 3.814012590377995*^9, {
   3.814012734092937*^9, 3.814012741815201*^9}},
 CellLabel->"Out[19]=",ExpressionUUID->"5304c8b9-a36e-488b-a876-05a2f148a302"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"Manipulate", "[", 
   RowBox[{
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        FractionBox["1", 
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           FractionBox[
            RowBox[{"(", 
             RowBox[{"x", "-", "1"}], ")"}], 
            RowBox[{"0.01", "k"}]]], "+", "1"}]], ",", 
        FractionBox["1", 
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           FractionBox[
            RowBox[{"(", 
             RowBox[{"x", "-", "1"}], ")"}], 
            RowBox[{"100", "k"}]]], "+", "1"}]]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "10"}], "}"}], ",", 
      RowBox[{"PlotRange", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"0", ",", "10"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "1"}], "}"}]}], "}"}]}], ",", 
      RowBox[{"PlotLegends", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"\"\<Low Temperature\>\"", ",", "\"\<High Temperature\>\""}], 
        "}"}]}], ",", 
      RowBox[{"PlotLabel", "\[Rule]", "\"\<Fermi-Dirac Distributions\>\""}], 
      ",", 
      RowBox[{"AxesLabel", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"\"\<E\>\"", ",", "\"\<<n>\>\""}], "}"}]}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "1", ",", "100"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8138353556724043`*^9, 3.813835395930387*^9}, {
  3.81401245453233*^9, 3.814012454709565*^9}, {3.814012530507678*^9, 
  3.8140125823690567`*^9}, {3.814012747332779*^9, 3.8140127564852047`*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"23ccefc2-2638-402d-9dfb-3558b81ca4cb"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`k$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`k$$], 1, 100}}, Typeset`size$$ = {511., {124., 128.}},
     Typeset`update$$ = 0, Typeset`initDone$$, Typeset`skipInitDone$$ = True}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`k$$ = 1}, 
      "ControllerVariables" :> {}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[{1/(E^(($CellContext`x - 1)/(0.01 $CellContext`k$$)) + 1), 1/(
         E^(($CellContext`x - 1)/(100 $CellContext`k$$)) + 
         1)}, {$CellContext`x, 0, 10}, PlotRange -> {{0, 10}, {0, 1}}, 
        PlotLegends -> {"Low Temperature", "High Temperature"}, PlotLabel -> 
        "Fermi-Dirac Distributions", AxesLabel -> {"E", "<n>"}], 
      "Specifications" :> {{$CellContext`k$$, 1, 100}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{556., {169., 175.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.81383538025928*^9, 3.8138353961495323`*^9}, 
   3.814012560682835*^9, 3.814012592443865*^9, {3.8140127500280724`*^9, 
   3.81401275687386*^9}},
 CellLabel->"Out[22]=",ExpressionUUID->"5be63bf4-6da1-4921-ac1a-59372c1c115e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", "\[IndentingNewLine]", 
  
  RowBox[{"Manipulate", "[", 
   RowBox[{
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        FractionBox["1", 
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           FractionBox[
            RowBox[{"(", 
             RowBox[{"x", "-", "1"}], ")"}], 
            RowBox[{"0.1", "k"}]]], "-", "1"}]], ",", 
        FractionBox["1", 
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           FractionBox[
            RowBox[{"(", 
             RowBox[{"x", "-", "1"}], ")"}], 
            RowBox[{"1000", "k"}]]], "-", "1"}]]}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"x", ",", "0", ",", "10"}], "}"}], ",", 
      RowBox[{"PlotRange", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"0", ",", "10"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{"0", ",", "500"}], "}"}]}], "}"}]}], ",", 
      RowBox[{"PlotLegends", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"\"\<Low Temperature\>\"", ",", "\"\<High Temperature\>\""}], 
        "}"}]}], ",", 
      RowBox[{"PlotLabel", "\[Rule]", "\"\<Bose-Einstein Distributions\>\""}],
       ",", 
      RowBox[{"AxesLabel", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{"\"\<E\>\"", ",", "\"\<<n>\>\""}], "}"}]}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"k", ",", "1", ",", "100"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8138354257180367`*^9, 3.813835453664855*^9}, {
   3.8140124571404667`*^9, 3.814012457341323*^9}, 3.8140125350120907`*^9, {
   3.8140125723608923`*^9, 3.8140125756411*^9}, {3.814012763956237*^9, 
   3.814012797652313*^9}, {3.81401290031374*^9, 3.8140129078569307`*^9}},
 CellLabel->"In[29]:=",ExpressionUUID->"ca3eb8b8-e20b-4852-b9b6-786518036c3a"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`k$$ = 1, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`k$$], 1, 100}}, Typeset`size$$ = {511., {123., 127.}},
     Typeset`update$$ = 0, Typeset`initDone$$, Typeset`skipInitDone$$ = True}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`k$$ = 1}, 
      "ControllerVariables" :> {}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[{1/(E^(($CellContext`x - 1)/(0.1 $CellContext`k$$)) - 1), 1/(
         E^(($CellContext`x - 1)/(1000 $CellContext`k$$)) - 
         1)}, {$CellContext`x, 0, 10}, PlotRange -> {{0, 10}, {0, 500}}, 
        PlotLegends -> {"Low Temperature", "High Temperature"}, PlotLabel -> 
        "Bose-Einstein Distributions", AxesLabel -> {"E", "<n>"}], 
      "Specifications" :> {{$CellContext`k$$, 1, 100}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{556., {168., 174.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.813835434308309*^9, 3.813835454298745*^9}, 
   3.8140125947868958`*^9, {3.814012766190415*^9, 3.814012798853034*^9}, {
   3.814012900978652*^9, 3.814012908459283*^9}},
 CellLabel->"Out[29]=",ExpressionUUID->"444e8ec4-4d4a-4ea0-9e64-8285950b2bac"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.814012602850031*^9, 
  3.814012605935334*^9}},ExpressionUUID->"5616cfcf-a323-46a1-9f4a-\
f783c1654d2b"]
},
WindowSize->{808, 855},
WindowMargins->{{Automatic, 310}, {Automatic, 180}},
FrontEndVersion->"12.1 for Mac OS X x86 (64-bit) (June 19, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"c7ad5309-132d-4a67-ba22-4a384a02ba55"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1764, 49, 131, "Input",ExpressionUUID->"06a9b235-62d8-4a55-80ce-eb744f449acb"],
Cell[2347, 73, 2190, 42, 345, "Output",ExpressionUUID->"4922b6a0-92f7-4c27-9452-38b6bf9eebfa"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4574, 120, 1475, 38, 93, "Input",ExpressionUUID->"e1b2e79d-4bff-4fae-b653-0bc54b962a4d"],
Cell[6052, 160, 1986, 38, 363, "Output",ExpressionUUID->"5304c8b9-a36e-488b-a876-05a2f148a302"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8075, 203, 1755, 45, 152, "Input",ExpressionUUID->"23ccefc2-2638-402d-9dfb-3558b81ca4cb"],
Cell[9833, 250, 1991, 38, 386, "Output",ExpressionUUID->"5be63bf4-6da1-4921-ac1a-59372c1c115e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11861, 293, 1861, 47, 173, "Input",ExpressionUUID->"ca3eb8b8-e20b-4852-b9b6-786518036c3a"],
Cell[13725, 342, 2019, 38, 361, "Output",ExpressionUUID->"444e8ec4-4d4a-4ea0-9e64-8285950b2bac"]
}, Open  ]],
Cell[15759, 383, 152, 3, 30, "Input",ExpressionUUID->"5616cfcf-a323-46a1-9f4a-f783c1654d2b"]
}
]
*)

